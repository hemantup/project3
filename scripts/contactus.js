
myApp.controller("contactus", ["$scope", "geoipService", "googleService", "myGeolocation", function ($scope, geoipService, googleService, myGeolocation) {
    $scope.modelData = [];
    googleService.myMappy = null;
    geoipService.getIPLoc().then(function (res) {
        var fhsulat = 38.8737984;
        var fhsulon = -99.3399948;
        $scope.modelData.latitude = fhsulat;
        $scope.modelData.longitude = fhsulon;
        googleService.setmarker(fhsulat, fhsulon, "gppgle2", "FHSU", "FHSU", "//maps.google.com/mapfiles/kml/shapes/open-diamond.png", true, 7);

      myGeolocation.getLocation().then(function (brloc) {

          $scope.modelData.brlatitude = brloc.latitude;
          $scope.modelData.brongitude = brloc.longitude;
          googleService.setmarker(brloc.latitude, brloc.longitude, "gppgle2", "Device", "Browser", "//maps.google.com/mapfiles/kml/pal3/icon56.png", false, 14);
          googleService.tryDirections(
              brloc.latitude,
              brloc.longitude,
              "gppgle2",
              14,
              fhsulat,
              fhsulon
              );


      });


    });
   
                                }]);
         
        