/*
1. Reset modelData which creates table in the view.
2. Reset myMappy variable of custom service: googleService which holds map object.
3. Get user's IP address along with latitude and longitude using custom geoipService.
3.1. if geoipService fails->call myGeolocation to get current location of browser,-> if myGeolocation succeed ->googleService.setmarker to set location by browser on gogole map. 
3.2 if geoipService succeed -> use googleService.setmarker to set IP location on google maps, -> myGeolocation.getLocation to get browser location if myGeolocation succeed -> googleService.setmarker to set browser location & calculate distance using googleService.tryDirections 

All the above actions are on initialization of controller. When user will be clicking on link in navigation, all the above steps will execute, and view will be loaded in index.html.
*/
myApp.controller("visitorlocation", ["$scope", "geoipService", "googleService", "myGeolocation", function ($scope, geoipService, googleService, myGeolocation) {
    $scope.modelData = [];
    googleService.myMappy = null;
  geoipService.getIPLoc().then(function(res) {
        if (res.status === 0) {
            res.ip = "Unable to find.";
            $scope.modelData = res;

            myGeolocation.getLocation().then(function (brloc) {

                $scope.modelData.brlatitude = brloc.latitude;
                $scope.modelData.brongitude = brloc.longitude;

                googleService.setmarker(brloc.latitude, brloc.longitude, "gppgle2", "Device", "Browser", "//maps.google.com/mapfiles/kml/pal3/icon56.png", false, 14);
            });
        }
        
        else {
            $scope.modelData = res;
            googleService.setmarker(res.latitude, res.longitude, "gppgle2", "Internet", "Internet", "//maps.google.com/mapfiles/kml/shapes/info-i_maps.png", true, 7);

            myGeolocation.getLocation().then(function (brloc) {
               
                $scope.modelData.brlatitude = brloc.latitude;
                $scope.modelData.brongitude = brloc.longitude;
                googleService.setmarker(brloc.latitude, brloc.longitude, "gppgle2", "Device", "Browser", "//maps.google.com/mapfiles/kml/pal3/icon56.png", false, 14);
                googleService.tryDirections(
                    brloc.latitude,
                    brloc.longitude,
                    "gppgle2",
                    14,
                    res.latitude,
                    res.longitude
                    );


            });

            
        }


    });
   
                                }]);
         
        