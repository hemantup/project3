/*
Talks to  external geoIP service to get IP and details like latitude/longitude.

sample response from external service is:
{
"ip":"209.114.127.19",
"country_code":"US",
"country_name":"United States",
"region_code":"KS",
"region_name":"Kansas",
"city":"Hays",
"zip_code":"67601",
"time_zone":"America/Chicago",
"latitude":38.9001,
"longitude":-99.347,
"metro_code":678
}

if nothing found then external service returns 404 page not found.

"geoipService" created here adds status = 1 for success and 0 for failure on top of response recived from external service.



$http angular inbuilt service is used to put call to external service.
$log angular inbuilt service is used to put message in console in browser.


if domip is null, means you want to get lat+lon for requestor's IP.

*/
myApp.factory("geoipService", function($http, $log) {
    return {
        getIPLoc: function(domip) {
            var servurl = "//freegeoip.net/json/";
            if (domip) {
                servurl += domip;
            }
            $log.info("geoip call will be sent to::: " + servurl);
            return $http.get(servurl).then(function(response) {
                $log.info("Positive Response from GeoIP external source::: " + JSON.stringify(response));
                response.data.status = (response.status === 200) ? 1 : 0;
                return response.data;
            }, function(reason) {
                $log.error("Error Response from GeoIP external source::: " + JSON.stringify(reason));
                return $.parseJSON('{ "status": 0 }');

            });
        }
    };
});

/*
"googleService" talks to external Google map API's , reference for which is added in index.html.

variable myMappy: is used to maintain map object in b/w multiple calls from same controller, on initialization of controller consuming "googleService", it is set to null.

setmarker function is used to set a marker on google map,
parameters:
lat: latitude of marker
lon: longitude of marker
mapdivId: element id of the div where map needs to be rendered
ttl: title of marker
lbl: label of marker
iconurl: url to be used for marker
bnc: if true, marker keeps on bouncing otherwise drops on location
zooom: initial zomm level for the google map

tryDirections: this method must be called when source and destination has been calculated, I could use promises for this purpose, in service calls calculating location of source and destination. bottom line is, I must have valid source and destination when I reach here in this function call.
This function assumes, you always travel driving :)
parameters:
brlat: source latitude,
brlon: source longitude,
mapdivId: element id of the div where map needs to be rendered
zooom: initial zomm level for the google map
tgtlat: destination latitude
tgtlon: destination longitude

google.maps.DirectionsService Instantiate a directions service
google.maps.LatLngBounds is used just to take both source and destination in viewable zon on map on page.

 */
myApp.factory("googleService", function() {
    return {
        myMappy: null,
        setmarker: function(lat, lon, mapdivId, ttl, lbl, iconurl, bnc, zooom) {
            var theCenter = new google.maps.LatLng(lat, lon);
            var mapOptions = {
                zoom: zooom,
                center: theCenter
            };

            if (this.myMappy) {
                this.myMappy.setOptions(mapOptions);
            } else {
                var mapOptions = {
                    zoom: zooom,
                    center: theCenter
                };
                this.myMappy = new google.maps.Map(document.getElementById(mapdivId), mapOptions);

            }

            var marker = new google.maps.Marker({
                position: theCenter,
                title: ttl,
                label: lbl,
                icon: iconurl,
                animation: bnc ? google.maps.Animation.BOUNCE : google.maps.Animation.DROP,
                map: this.myMappy
            });
        },
        tryDirections: function(brlat, brlon, mapdivId, zooom, tgtlat, tgtlon) {
            if (brlat && brlon && tgtlat && tgtlon) {
                var theCenter = new google.maps.LatLng(brlat, brlon);
                var theTgt = new google.maps.LatLng(tgtlat, tgtlon);

                var mapOptions = {
                    zoom: zooom,
                    center: theCenter
                };

                if (this.myMappy) {
                    this.myMappy.setOptions(mapOptions);
                } else {
                    var mapOptions = {
                        zoom: zooom,
                        center: theCenter
                    };
                    this.myMappy = new google.maps.Map(document.getElementById(mapdivId), mapOptions);

                }

                var bounds = new google.maps.LatLngBounds();
                bounds.extend(theCenter);
                bounds.extend(theTgt);
                this.myMappy.fitBounds(bounds);
                google.maps.event.trigger(this.myMappy, 'resize');

                var directionsService = new google.maps.DirectionsService;
                var directionsDisplay = new google.maps.DirectionsRenderer({
                    map: this.myMappy
                });

                directionsService.route({
                    origin: theCenter,
                    destination: theTgt,
                    avoidTolls: true,
                    avoidHighways: false,
                    travelMode: google.maps.TravelMode.DRIVING
                }, function(response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                    } else {

                        window.alert("Directions request failed due to " + status);
                    }
                });
            }
        }
    };
});

/**
 "myGeolocation" is used to get latitude and longitude of user's browser. 
 $q is an inbuilt angularJS service and I have used here for promises.
 I could avoid using $window (inbuilt angularJS service ) and instead use window directly.

 Why I wanted to use promise here, this is very common scenerio on mobiles that location service is disabled for browser by user 
 or user say, I don't want to give my location. In that case, I wanted to avoid calls to googleService.tryDirections which needs both source and destination essentially to get directions.
 myGeolocation.getLocation retuns json object like  {latitude: 38.8737984, longitude: -99.3399948}
 */
myApp.factory("myGeolocation", function($q, $window) {
    return {
        getLocation: function() {
            var deferred = $q.defer();

            if ($window.navigator && $window.navigator.geolocation) {
                $window.navigator.geolocation.getCurrentPosition(
                    function(position) {

                        var pos = {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude
                        };

                        deferred.resolve(pos);


                    }, function() {
                        deferred.reject("Error: The Geolocation service failed.");
                    }
                );
            } else {
                // Browser doesn't support Geolocation
                deferred.reject("Error: Your browser doesn\'t support geolocation.");
            }

            return deferred.promise;
        }
    };
});
