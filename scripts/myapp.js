
        var myApp = angular.module("testingapp", ['ngRoute']);
		
		
		
		myApp.config(['$routeProvider',  '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
   
        .when('/yourlocation', { templateUrl: 'parts/visitorlocation.html'})     
		.when('/searchanip', { templateUrl: 'parts/searchip.html'}) 
      .when('/intro', { templateUrl: 'parts/introduction.html' })
           .when('/contactus', { templateUrl: 'parts/contactus.html' })
        .otherwise({ redirectTo: '/intro' });
   
		}]);

		