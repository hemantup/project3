/*
In function callTheMagic:
1. Reset modelData which creates table in the view.
2. Reset myMappy variable of custom service: googleService which holds map object.
searchip.queryExecuted is set to false to hide the map and table part of view.
If the input field is blank then return.

3. Get latitude and longitude of domainname using custom geoipService which calls freegeoip.net/json/{IP_or_hostname}.
3.1. if geoipService fails->call myGeolocation to get current location of browser,-> if myGeolocation succeed ->googleService.setmarker to set location by browser on gogole map. 
3.2 if geoipService succeed -> use googleService.setmarker to set IP location on google maps, -> myGeolocation.getLocation to get browser location if myGeolocation succeed -> googleService.setmarker to set browser location & calculate distance using googleService.tryDirections 

queryExecuted is set to tru in both cases 3.1 and 3.2

*/
myApp.controller("searchip", 
  ["$scope", "geoipService", "googleService", "myGeolocation", function ($scope, geoipService, googleService, myGeolocation) {
        $scope.modelData = [];
        googleService.myMappy = null;
        $scope.searchip = {
            queryExecuted : false,
            callTheMagic: function () {
                $scope.modelData = [];
                googleService.myMappy = null;
                $scope.searchip.queryExecuted = false;
                if ($('#domin_ip').val() === '') return;
                geoipService.getIPLoc($('#domin_ip').val()).then(function (res) {
                    if (res.status === 0) {
                        res.ip = "Unable to find.";
                        $scope.modelData = res;

                        myGeolocation.getLocation().then(function (brloc) {

                            $scope.modelData.brlatitude = brloc.latitude;
                            $scope.modelData.brongitude = brloc.longitude;

                            googleService.setmarker(brloc.latitude, brloc.longitude, "gppgle2", "Device", "Browser", "//maps.google.com/mapfiles/kml/pal3/icon56.png", false, 14);
                        });
                        $scope.searchip.queryExecuted = true;
                    }

                    else {
                        $scope.modelData = res;
                        $scope.modelData.inputd_p = $('#domin_ip').val();
                        $scope.searchip.queryExecuted = true;
                        $('#domin_ip').val("");
                        
                        googleService.setmarker(res.latitude, res.longitude, "gppgle2", "Hosting", "Hosting", "//maps.google.com/mapfiles/kml/pal4/icon21.png", true, 7);

                        myGeolocation.getLocation().then(function (brloc) {

                            $scope.modelData.brlatitude = brloc.latitude;
                            $scope.modelData.brongitude = brloc.longitude;
                            googleService.setmarker(brloc.latitude, brloc.longitude, "gppgle2", "Device", "Browser", "//maps.google.com/mapfiles/kml/pal3/icon56.png", false, 14);
                            googleService.tryDirections(
                                brloc.latitude,
                                brloc.longitude,
                                "gppgle2",
                                14,
                                res.latitude,
                                res.longitude
                                );


                        });


                    }


                });
            }
    };
		

    }]);




    


                       